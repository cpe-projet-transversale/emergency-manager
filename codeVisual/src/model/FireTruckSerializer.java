package model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class FireTruckSerializer extends StdSerializer<FireTruck> {
    public FireTruckSerializer() {
        this(null);
    }

    public FireTruckSerializer(Class<FireTruck> t){
        super(t);
    }

    @Override
    public void serialize(FireTruck value, JsonGenerator jgen, SerializerProvider provider)
    throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        /*jgen.writeNumberField("lat", value.getPos().getLatitude());
        jgen.writeNumberField("long", value.getPos().getLongitude());*/
        jgen.writeNumberField("fireStationId", value.assignedFireStationId);
        if(value.emergencyId == 0)
            jgen.writeNullField("emergencyId");
        else
            jgen.writeNumberField("emergencyId", value.emergencyId);
        jgen.writeEndObject();
    }
}