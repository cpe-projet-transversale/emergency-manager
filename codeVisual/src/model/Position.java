package model;

public class Position {
    private double latitude;
    private double longitude;

    public Position(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Position){//Si on compare avec une autre position
            return (
                    (double)((Position)obj).getLatitude() == (double)this.getLatitude() &&
                    (double)((Position)obj).getLongitude() == (double)this.getLongitude()
                ); //Compare lat/long
        }
        return super.equals(obj); //Sinon comparaison habituelle
    }

    @Override
    public String toString(){
        return "Lat/Long : " + this.getLatitude() + " ; "  + this.getLongitude();
    }
}
