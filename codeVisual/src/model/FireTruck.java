package model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = FireTruckSerializer.class) //permet d'indiquer la classe qui transforme l'objet en json 
@JsonIgnoreProperties(ignoreUnknown = true) //Permet d'ignorer les inconnus en provenance du JSON
public class FireTruck extends Service {

	int id;
	//Intervention assignedIntervention = null;
	int assignedFireStationId;
	//List<Crew> assignedCrew = new ArrayList<Crew>();
	boolean returnToBase;
	int emergencyId;
/*
	public void setFree() {
		this.setIntervention(null);
	}*/

	public boolean getReturnToBase() {
		return this.returnToBase;
	}

	public int getId() {
		return this.id;
	}

	public int getAssignedFireStationId() {
		return this.assignedFireStationId;
	}


	public Position getPos() {
		return this.position;
	}

	public int getEmergencyId(){
		return this.emergencyId;
	}

/*	public List<Crew> getCrew() {
		return this.assignedCrew;
	}
/*
	public void assignNecessaryCrew() {
		Crew.getFreeCrewFromFireStation(this.assignedFireStation.getId());
	}*/
/*
	public Intervention getIntervention() {
		return this.assignedIntervention;
	}*//*

	public void setIntervention(Intervention intervention){
		this.assignedIntervention = intervention;
	}*/

	public void moveTo(Position pos) {
		this.position = pos;
	}
/*
	public static List<FireTruck> getAllFireTruckForIntervention(int id) {
		Request.request(null, "GET");
		//SELECT * FROM FireTruck WHERE assignedIntervention LIKE id
		return null;
	}

	public static List<FireTruck> getTrucksWithCrew() {
		Request.request(null, "GET");
		//SELECT * FROM FireTruck WHERE id not in (SELECT * FROM Crew WHERE associatedFireTruckId IS NOT NULL)
		return null;
	}

	public static List<FireTruck> getAllFireTruck() {
		Request.request(null, "GET");
		return null;
	}*/

	@JsonCreator // constructor can be public, private, whatever
	private FireTruck(
        @JsonProperty("id") int id,
        @JsonProperty("lat") double latitude,
		@JsonProperty("long") double longitude,
		@JsonProperty("fireStationId") int fireStationId,
		@JsonProperty("emergencyId") int emergencyId)
	{
		this.id = id;
		this.position = new Position(latitude, longitude);
		this.assignedFireStationId = fireStationId;
		this.emergencyId = emergencyId;
	}

	public void setEmergencyId(int eId) {
		this.emergencyId = eId;
	}

}
