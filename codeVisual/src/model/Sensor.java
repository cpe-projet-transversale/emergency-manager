package model;

import java.util.List;
import com.fasterxml.jackson.annotation.*;
import utils.Request;

public class Sensor {
	int id;
    Position position;
    int intensity;

    /**
     * Retourne la position du capteur
     * @return Position : latitude/longitude du capteur
     */
	public Position getPos() {
		return position;
	}

    /**
     * Retourne le nombre de camion nécessaire à l'intervention en fonction des seuils
     * @return int Le nombre de camion nécessaire
     */
	/*public int getNeedInFireTruck() {
		return (int) this.intensity;
	}*/

    /**
     * Récupère le capteur correspondant à l'ID fourni
     * @param randomSensorId l'ID du capteur généré aléatoirement
     * @return Sensor le capteur correspondant à l'ID
     */
	/*public static Sensor getSensorById(int randomSensorId) {
		return null;
	}*/

	public int getIntensity() {
		return this.intensity;
	}

	public void setIntensity(int newIntensity) {
        this.intensity = newIntensity;
	}

	/*public static List<Sensor> getSensorsWithProblems() {
		Request.request(null, "GET");
        //SELECT * FROM Sensor WHERE intensity > 0 and id NOT IN (SELECT sensorId FROM Intervention)
		return null;
	}

	public static int getNumberOfSensors() {
		Request.request(null, "GET");
		//SELECT count(1) FROM Sensor;
		//For each (object) count++ 
		return 0;
	}*/

	public int getId() {
		return this.id;
	}
    /*@Test
	public void whenSerializingUsingJsonAnyGetter_thenCorrect()
	throws JsonProcessingException {
	
		//A utiliser pour la position
		ExtendableBean bean = new ExtendableBean("My bean");
		bean.add("attr1", "val1");
		bean.add("attr2", "val2");
	
		String result = new ObjectMapper().writeValueAsString(bean);
	
		assertThat(result, containsString("attr1"));
		assertThat(result, containsString("val1"));
	}*/

	@JsonCreator // constructor can be public, private, whatever
	private Sensor(
		@JsonProperty("id") int id,
		@JsonProperty("lat") double latitude,
		@JsonProperty("long") double longitude,
		@JsonProperty("value") int intensity)
	{
		this.id = id;
		this.position = new Position(latitude, longitude);
		this.intensity = intensity;
	}
}