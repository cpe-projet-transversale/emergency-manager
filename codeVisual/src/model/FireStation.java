package model;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(ignoreUnknown = true) //Permet d'ignorer les inconnus en provenance du JSON
public class FireStation extends Service {

	/**
	 * L'id de la caserne
	 */
	int id;
	String name;

	public Position getPos() {
		return this.position;
	}


	@JsonCreator // constructor can be public, private, whatever
	private FireStation(
        @JsonProperty("id") int id,
        @JsonProperty("lat") double latitude,
		@JsonProperty("long") double longitude,
		@JsonProperty("name") String name)
	{
		this.id = id;
		this.position = new Position(latitude, longitude);
        this.name = name;
	}

	/**
	 * Fourni un camion disponible. Sinon renvoi null
	 * @return FireTruck ou NULL
	 */
	public FireTruck getFreeFireTruck() {
		/*for (FireTruck fireTruck : associatedFireTruck) {
			if(fireTruck.getPos() == this.position){
				return fireTruck;
			}
		}*/
		return null;
	}

	public int getId() {
		return this.id;
	}

}
