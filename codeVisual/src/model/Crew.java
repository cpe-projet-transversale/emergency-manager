package model;

import utils.Request;

public class Crew {

	/**
	 * Le pompier/l'équipe est assigné à un camion lors d'une intervention
	 */
	FireTruck associatedFireTruck;
	/**
	 * Le pompier/l'équipe est rattaché à une caserne.
	 */
	FireStation associatedFireStation;

	/**
	 * On libère le pompier/l'équipe de son affectation à un camion
	 */
	public void setFree() {
		this.setFireTruck(null);
	}

	/**
	 * 
	 * @return FireStation le camion associé au pompier/l'équipe
	 */
	public FireStation getFireStation(){
		return this.associatedFireStation;
	}

	public void setFireTruck(FireTruck fireTruck){
		this.associatedFireTruck = fireTruck;
	}

	/**
	 * Permet, pour une caserne donnée, d'obtenir une équipe/un pompier disponible dans cette caserne
	 * @param assignedFireStationId l'ID de la caserne dont l'on veut savoir si une équipe/un pompier est disponible
	 * @return Crew une équipe/un pompier disponible
	 */
	public static Crew getFreeCrewFromFireStation(int assignedFireStationId) {
		//Request.request(null, "GET");
		//SELECT * FROM Crew WHERE associatedFireTruck is NULL AND associatedFireStation LIKE assignedFireStationId LIMIT 1
		return null;
	}

}
