package model;

import java.util.List;

import utils.Request;

public class Intervention {

    private int id;
    private Position position;
	//private Sensor sensor; 
	private int intensity;

	/*public float getIntensity() {
		return this.getSensor().getIntensity();
	}*/

	public int getIntensity() {
		return this.intensity;
	}

	public void setIntensity(int intensity) {
		this.intensity = intensity;
	}

	public void close() {
        //To be done
		//Request.request(null, "DELETE");
        //DELETE FROM Intervention WHERE id like this.id ;
        //this.destroy
	}

	public void setPos(Position pos) {
        this.position = pos;
	}

	/*public void setSensor(Sensor sensor) {
        this.sensor = sensor;
	}

	/*public void addAssignedTruck(FireTruck truck) {
        this.assignedFireTruck.add(truck);
	}*/

	/*public Sensor getSensor() {
		return this.sensor;
	}*/

	public Position getPos() {
		return this.position;
	}

	public int getId() {
		return id;
	}

	public static List<Intervention> getOnGoingInterventions() {
		return null;
	}

}
