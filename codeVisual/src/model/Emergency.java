package model;

import java.io.IOException;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonSerialize(using = EmergencySerializer.class) //permet d'indiquer la classe qui transforme l'objet en json 
@JsonIgnoreProperties(ignoreUnknown = true) //Permet d'ignorer les inconnus en provenance du JSON
public class Emergency {
    private int id;
    private Position position;
    private String type;
    private int intensity;
    private boolean anInterventionExist;
    public boolean isInDb;


    public Emergency(int id, Position position, int intensity) {
        this.id = id;
        this.position = position;
        this.intensity = intensity;
        this.type = "fire";
        this.anInterventionExist = false;
        this.isInDb = false;
    }


	@JsonCreator // constructor can be public, private, whatever
	private Emergency(
        @JsonProperty("id") int id,
        @JsonProperty("lat") double latitude,
        @JsonProperty("long") double longitude,
        @JsonProperty("value") int intensity,
        @JsonProperty("type") String type)
	{
		this.id = id;
		this.position = new Position(latitude, longitude);
        this.intensity = intensity;
        this.type = type;
        this.anInterventionExist = false;
        this.isInDb = true;
	}

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Position getPosition() {
        return this.position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setAnInterventionExist(boolean bool){
        this.anInterventionExist = bool;
    }

    public int getIntensity() {
        return this.intensity;
    }

    public boolean getAnInterventionExist() {
        return this.anInterventionExist;
    }


	public void setIntensity(int i) {
        this.intensity = i;
	}

}
