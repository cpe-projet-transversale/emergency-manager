package task;

import java.util.TimerTask;

import com.fasterxml.jackson.core.JsonProcessingException;

import controller.GameManager;
import utils.Request;


public class updateDatabase extends TimerTask {
    private GameManager gameManager;

    public updateDatabase(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public void run() {
        System.out.println("*** MAJ BDD ***");
        try {
            Request.patchFireTrucksAssociations(gameManager.getListFireTruck());
            Request.postEmergency(gameManager.getListEmergency());
            gameManager.updateListSensorIntensity(Request.getSensorList());
            
            //gameManager.setListEmergency(Request.getEmergencyList()); //ok
        } catch (JsonProcessingException e) {
            System.out.println("Failed to update DB");
            e.printStackTrace();
        }
        System.out.println("--- TURN ENDS ---");
       /* System.out.println("*** STATE ***");
        List<Emergency> list = gameManager.getListEmergency();
        for (Emergency emergency : list) {
            System.out.println(emergency.getId() + " : " + emergency.getIntensity());
        }
        System.out.println("*** STATE ***");*/
    }
}