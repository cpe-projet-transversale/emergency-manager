package task;

import java.util.TimerTask;

import com.fasterxml.jackson.core.JsonProcessingException;

import controller.GameManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Emergency;
import model.FireStation;
import model.FireTruck;
import model.Intervention;
import model.Position;
import model.Sensor;
import model.Service;
import utils.Request;

public class checkNewFire extends TimerTask {
    GameManager gameManager;

    public checkNewFire(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public void run() {
        Map<Sensor, Emergency> mapSensorEmergency = gameManager.getMapSensorEmergency();
        System.out.println(mapSensorEmergency.size());
        for (Sensor s : gameManager.getListSensor()) {
            // On maj l'intensité des feux qui ont leur intensité modifiées
            if (mapSensorEmergency.containsKey(s)) {
               
                if(s.getIntensity() != (mapSensorEmergency.get(s)).getIntensity() ){
                    System.out.println("Maj intensity emergency " + mapSensorEmergency.get(s).getId() + " with sensor " + s.getId() + " INTENSITY " + s.getIntensity());
                    (mapSensorEmergency.get(s)).setIntensity(s.getIntensity());// On récupère le feu associé au sensor pour patch son intensité par celle du sensor
                }
                if (s.getIntensity() == 0) {
                    // L'emergency sera supprimée de la bdd à la maj de la bdd
                    /*
                     * gameManager.getListEmergency().remove(mapSensorEmergency.get(s)); //On remove
                     * l'emergency
                     */
                    mapSensorEmergency.remove(s); // On remove la key/value de la map
                    System.out.println("Remove map : sensor " + s.getId());
                }
            }
            // On check l'intensité des capteurs, et si ils sont != 0 & qu'il n'y a pas
            // encore de feu dessus, on crée un feu
            else if (s.getIntensity() != 0) {
                Emergency e = new Emergency(0, s.getPos(), s.getIntensity());
                gameManager.getListEmergency().add(e);
                mapSensorEmergency.put(s, e);
                System.out.println("Adding map : sensor "+s.getId() + " to emergency "+e.getId());
            }
            
        }

        //On récupère la liste des camions disponible & prépare la liste des emergencies à traiter
        List<Integer> onGoingEmergency = new ArrayList<>();
        List<FireTruck> availableFireTruck = new ArrayList<>();
        for(FireTruck ft : gameManager.getListFireTruck()){
            //System.out.println(ft.getId() + " est en BDD associé à " + ft.getEmergencyId());
            if(ft.getEmergencyId() !=0) //Si le camion est affecté à un feu
                onGoingEmergency.add(ft.getEmergencyId()); //On ajoute toutes les emergencies qui ont des camions associés
            else {//si il est dispo
                availableFireTruck.add(ft); //on ajoute le camion aux camions dispos
                System.out.println("Le camion est dispo : " + ft.getId());
            }
        }  

        //On parcourt la liste des emergencies à la recherche de celles qui n'ont pas de camions affectés, pour en affecter
        for(Emergency e : gameManager.getListEmergency()){ //On parcourt les emergencies
            if(availableFireTruck.isEmpty())break; //Si on a plus de camions, on ne peut plus affecter, donc on quitte

            if(onGoingEmergency.contains(e.getId())){ //Si l'émergency est contenue dans la liste, alors un camion lui est affectée
                
            }
            else{
                if(e.getId() != 0){ //il faut que l'emergency soit en BDD
                    FireTruck ft = availableFireTruck.get(0);
                    ft.setEmergencyId(e.getId()); //on associe le camion à un feu
                    availableFireTruck.remove(ft); //le camion est associé à un feu, il n'est plus dispo
                    onGoingEmergency.add(e.getId());
                    System.out.println("Modif emergency : " + e.getId() + " pour camion " + ft.getId());
                }
            }
        }

        /*for (Emergency emergency : gameManager.getListEmergency()) {
            if(!emergency.getAnInterventionExist()){ //Si aucune intervention existe
                
                Intervention intervention = new Intervention(0, emergency.getPosition()); //On crée l'intervention
                for(FireTruck firetruck : gameManager.getListFireTruck()) {
                    if(firetruck.getReturnToBase()){
                        firetruck.setReturnToBase(false);
                        gameManager.getListIntervention().add(intervention);
                        gameManager.getMapInterventionFireTruck().add(intervention, fireTruck);
                        break;
                    }
                }

            }
        }*/
            //PEUT ETRE FAIRE PLUTOT EN FONCTION DES CAPTEURS, EN MODIFIANT LES INTENSITE DANS LE SIMULATEUR

            /*
        List<Sensor> sensors = Sensor.getSensorsWithProblems(); //On récupère les capteurs qui ont une intensité > 0 et qui n'ont pas d'interventions
        for (Sensor sensor : sensors) {
            Intervention intervention = new Intervention(); //On crée l'intervention
            Position pos = sensor.getPos(); //On récupère la localisation (lat/long) de l'incendie (ici capteur)
            intervention.setPos(pos); //On assigne la position à l'intervention
            intervention.setSensor(sensor); //On assigne le capteur (l'incendie)
            for(int i = 0 ; i < sensor.getNeedInFireTruck() ; i++){
                Service serv = pos.getNearest(); //On récupère le camion OU la caserne la plus proche
                FireTruck truck;
                if (serv instanceof FireStation) { //Si c'est une caserne
                    //checker si il y a un camion dans la caserne
                    truck = ((FireStation) serv).getFreeFireTruck(); //On récupère les camions disponibles
                    truck.assignNecessaryCrew(); //On lui assigne des pompiers disponibles
                    truck.setIntervention(intervention);
                }
                else if (serv instanceof FireTruck) { //Si c'est un camion
                    truck = (FireTruck) serv; //On ré-assigne le camion avant son retour à la caserne
                    truck.setIntervention(intervention);
                }
            }
        }*/
        System.out.println("I've checked all sensors and created all necessary interventions");
    }
}