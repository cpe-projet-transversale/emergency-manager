package task;

import java.util.TimerTask;

import controller.GameManager;

import java.util.ArrayList;
import java.util.List;

import model.Emergency;
import model.FireTruck;
import model.Intervention;



public class checkFireDeath extends TimerTask {    
    GameManager gameManager;

    public checkFireDeath(GameManager gameManager) {
        this.gameManager = gameManager;
    }
    @Override
    public void run() {
        List<Integer> onGoingEmergency = new ArrayList<>();
        for(FireTruck ft : gameManager.getListFireTruck()){
            if(ft.getEmergencyId() !=0){ //Si le camion est associé à une emergency
                if(!onGoingEmergency.contains(ft.getEmergencyId())) //Si il n'est pas déjà inclut dans la liste
                    onGoingEmergency.add(ft.getEmergencyId()); //On ajoute toutes les emergencies qui ont des camions associés
            }
        }  

        for(Emergency e : gameManager.getListEmergency()){ //On parcourt les emergencies
            if(onGoingEmergency.contains(e.getId())){ //Si l'émergency est contenue dans la liste, alors elle existe encore
                boolean b = onGoingEmergency.remove(Integer.valueOf(e.getId())); //Donc les camions doivent encore y être associées
                System.out.println("Emergency still exist : "+ e.getId() + " ; success : " + b );
            }
        }

        for(FireTruck ft : gameManager.getListFireTruck()){ //Pour chaque chamions
            if(onGoingEmergency.contains(ft.getEmergencyId())) { //Si leur intervention concerne une emergency qui n'existe plus
                ft.setEmergencyId(0); //retour à la base
                System.out.println("Removing Truck affiliation");
            }
        }
        //System.out.println("Checking all interventions done");
    }
}