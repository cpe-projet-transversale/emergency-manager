package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
/*import java.io.DataOutputStream;
import java.util.HashMap;
import java.util.Map;*/
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.*;

public class Request {
    /**
     * Enumération des méthodes possibles pour les requêtes
     */
    enum methodPossibility {
        GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, PATCH
    };

    static String address = "http://164.4.1.10:";
    static int port = 3000;

    private static void allowMethods(String... methods) {
        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            methodsField.setAccessible(true);

            String[] oldMethods = (String[]) methodsField.get(null);
            Set<String> methodsSet = new LinkedHashSet<>(Arrays.asList(oldMethods));
            methodsSet.addAll(Arrays.asList(methods));
            String[] newMethods = methodsSet.toArray(new String[0]);

            methodsField.set(null/*static field*/, newMethods);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Requête HTTP
     * 
     * @param String url_string l'URL pour la requête
     * @param String method la méthode utilisée pour la requête
     * @return
     */
    public static String request(String url_string, String method, String parameters) {
        // Check if method is correct
        boolean flag = false;
        for (methodPossibility c : methodPossibility.values()) {
            if (c.name().equals(method)) {
                flag = true;
            }
        }
        if (!flag) {
            System.out.println("Méthode de requête inexistante");
            return "";// If it is not a valid method, we do nothing
        }

        // Generate the request
        URL url;
        try {
            allowMethods("PATCH");
            url = new URL(url_string);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // Request Headers (peut servir pour sécuriser les connections)
            con.setRequestProperty("Content-Type", "application/json");

            // Timeout
            con.setConnectTimeout(5000); // 5 secondes
            con.setReadTimeout(5000);

            if(method =="DELETE")
                con.setRequestMethod(method);

            if (method == "POST" || method == "PATCH"){
                
                con.setDoOutput(true);
                // Execute command
                //if(method == "PATCH")
                //con.setRequestProperty("X-HTTP-Method-Override", "PATCH");
                con.setRequestMethod(method);
    

                if (parameters != ""){
                    //System.out.println("Paramètres détectés");
                
                    try(OutputStream os = con.getOutputStream()) {
                        byte[] input = parameters.getBytes("utf-8");
                        os.write(input, 0, input.length);
                        //System.out.println("Test");
               
                    } 
                }
                else {
                        System.out.println("Paramètres manquants...");
                        return null;
                }
            }
            
            
            // Read
            int status = con.getResponseCode();
            if(status > 299) System.out.println("Code retour requête : " + Integer.toString(status));

            Reader streamReader = null;

            if (status > 299) { // Si erreur
                streamReader = new InputStreamReader(con.getErrorStream()); // on récupère erreur
            } else {
                streamReader = new InputStreamReader(con.getInputStream());
            }

            BufferedReader in = new BufferedReader(streamReader);
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            if(status > 299) System.out.println(content.toString());
            return content.toString();

        } catch (MalformedURLException e) {
            System.out.println("URL malformé");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception");
            e.printStackTrace();
        }
        return "";
    }

    
    public static List<Sensor> getSensorList() {
        String json = request(Request.address+Request.port+"/sensors", "GET", ""); //get json
        // Create object from json
        ObjectMapper mapper = new ObjectMapper(); // create once, reuse
        try {
            List<Sensor> value = mapper.readValue(json, new TypeReference<List<Sensor>>(){});
            return value;

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


	public static List<Emergency> getEmergencyList() {
		String json = request(Request.address+Request.port+"/emergencies", "GET",""); //get json
        // Create object from json
        ObjectMapper mapper = new ObjectMapper(); // create once, reuse
        try {
            List<Emergency> value = mapper.readValue(json, new TypeReference<List<Emergency>>(){});
            return value;

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    } 
    
	public static List<FireStation> getFireStationList() {
		String json = request(Request.address+Request.port+"/fireStations", "GET",""); //get json
        // Create object from json
        ObjectMapper mapper = new ObjectMapper(); // create once, reuse
        try {
            List<FireStation> value = mapper.readValue(json, new TypeReference<List<FireStation>>(){});
            return value;

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
	}
    
	public static List<FireTruck> getFireTruckList() {
		String json = request(Request.address+Request.port+"/fireTrucks", "GET",""); //get json
        // Create object from json
        ObjectMapper mapper = new ObjectMapper(); // create once, reuse
        try {
            List<FireTruck> value = mapper.readValue(json, new TypeReference<List<FireTruck>>(){});
            return value;

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
	}


	public static Map<Emergency, ArrayList<FireTruck>> getEmergencyFireTruck(List<Emergency> listE, List<FireTruck> listFT) {
        //String jsonFT = request(Request.address+Request.port+"/fireTrucks", "GET",""); //get json
        String jsonE = request(Request.address+Request.port+"/emergencies", "GET",""); //get json
        
        Map<Emergency, ArrayList<FireTruck>> map = new HashMap<Emergency, ArrayList<FireTruck>> ();

        ObjectMapper mapper = new ObjectMapper(); // create once, reuse
        JsonNode root;
        try {
            root = mapper.readTree(jsonE);
            if(root.isArray()){ //Si le json est bien un tableau
                for (final JsonNode objNode : root){ //on parcourt chaque emergency
                    //On récupère l'emergency
                    int idEmergency = objNode.get("id").asInt(); 
                    Emergency emergency = null;
                    for (Emergency e : listE) {
                        if(e.getId() == idEmergency){
                            emergency = e;
                            break;
                        }
                    }
                    //On récupère tous les camions
                    JsonNode secondRoot = objNode.get("fireTrucksIds");
                    ArrayList<FireTruck> fireTruckList = new ArrayList<FireTruck>(); 
                    for (final JsonNode objNode2 : secondRoot){
                        int idFireTruck = objNode2.asInt(); //on récupère l'id
                        //on récupère l'objet correspondant dans la liste
                        for (FireTruck ft : listFT){
                            if(ft.getId() == idFireTruck) {
                                fireTruckList.add(ft); //on ajoute à la liste des camions associé au feu
                                break;
                            }
                        }
                    }
                    if(emergency != null){
                        map.put(emergency, fireTruckList); //On ajoute l'objet dans la map
                    }
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
		return map;
    } 
    

    public static void postEmergency(List<Emergency> listE) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectMapper om = new ObjectMapper();
        JsonNode root;
        List<Emergency> emergencyToRemove = new ArrayList<>();
        for (Emergency e : listE) {
            if(e.isInDb == false){
                String resp = request(Request.address+Request.port+"/emergencies/fires","POST",om.writeValueAsString(e));
                //Then on maj l'id
                root = mapper.readTree(resp);
                e.setId(root.get("id").asInt());
                e.isInDb = true;
                System.out.println("Added emergency " + e.getId());
            }   
            else {
                if (e.getIntensity()!=0){ //Si l'intensité est supérieure à 0, on patch
                    //System.out.println("Patch du feu "+e.getId() + " pour intensity " + e.getIntensity());
                    request(Request.address+Request.port+"/emergencies/fires/"+Integer.toString(e.getId()),"PATCH",om.writeValueAsString(e));
                }
                else { //Si intensité = 0 on supprime !
                    System.out.println("Suppression du feu "+e.getId());
                    request(Request.address+Request.port+"/emergencies/fires/"+Integer.toString(e.getId()),"DELETE",null);
                    //listE.remove(e);
                    emergencyToRemove.add(e);
                }
            }        
        }
        for(Emergency emergency : emergencyToRemove){
            listE.remove(emergency);  
        }
    }


	public static void patchFireTrucksAssociations(List<FireTruck> listFireTruck) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        for (FireTruck ft : listFireTruck) {
                request(Request.address+Request.port+"/fireTrucks/"+Integer.toString(ft.getId()),"PATCH",om.writeValueAsString(ft));   
        }
	}
}