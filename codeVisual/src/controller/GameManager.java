package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Crew;
import model.Emergency;
import model.FireStation;
import model.Intervention;
import model.Sensor;
import model.FireTruck;
import utils.Request;

public class GameManager {
    private List <Sensor> listSensor;
    //private List <Intervention> listIntervention;
    private List <Emergency> listEmergency;
    private List <FireTruck> listFireTruck;
    private List <FireStation> listFireStation;
    private Map <Emergency, ArrayList<FireTruck>> mapEmergencyFireTruck; 
    private Map <Sensor,Emergency> mapSensorEmergency;
    
    private int maxIntensity = 9;

    public GameManager() {
        listSensor = Request.getSensorList(); //ok
        listEmergency = Request.getEmergencyList(); //ok
        listFireStation = Request.getFireStationList(); //ok
        listFireTruck  = Request.getFireTruckList(); //ok
        mapSensorEmergency = new HashMap<>();
        for(Sensor s : listSensor){
            for (Emergency e : listEmergency){
                if (s.getPos().equals(e.getPosition())){
                    mapSensorEmergency.put(s,e);
                    System.out.println("Sensor "+s.getId() + " to Emergency " + e.getId() + " for intensity " + s.getIntensity() + " - " + e.getIntensity());
                }
            }
        }
        System.out.println("GameManager initialized.");
       // mapEmergencyFireTruck = Request.getEmergencyFireTruck(listEmergency,listFireTruck); 
    }

    public List<FireStation> getListFireStation() {
        return this.listFireStation;
    }

    public List<Sensor> getListSensor() {
        return this.listSensor;
    }

  /*  public List<Intervention> getListIntervention() {
        return this.listIntervention;
    }
*/
    public List<Emergency> getListEmergency() {
        return this.listEmergency;
    }

    public int getMaxIntensity() {
        return this.maxIntensity;
    }

    public List<FireTruck> getListFireTruck() {
        return this.listFireTruck;
    }

    public Map<Emergency,ArrayList<FireTruck>> getMapEmergencyFireTruck() {
        return this.mapEmergencyFireTruck;
    }

    public Map<Sensor,Emergency> getMapSensorEmergency() {
        return this.mapSensorEmergency;
    }


    
	public Sensor getSensorById(int randomSensorId) {
        for(Sensor sensor : this.listSensor) {
            if(sensor.getId() == randomSensorId) {
                return sensor;
            }
        }
        return null;
    }
    
	public FireTruck getFireTruckById(int fireTruckId) {
        for(FireTruck fireTruck : this.listFireTruck) {
            if(fireTruck.getId() == fireTruckId) {
                return fireTruck;
            }
        }
        return null;
    }
    
	public FireStation getFireStationById(int fireStationId) {
        for(FireStation fireStation : this.listFireStation) {
            if(fireStation.getId() == fireStationId) {
                return fireStation;
            }
        }
        return null;
	}

	public void setListEmergency(List<Emergency> emergencyList) {
        this.listEmergency = emergencyList;
	}

	public void updateListSensorIntensity(List<Sensor> sensorList) {
        for(Sensor s : this.listSensor){
            for(Sensor s2 : sensorList){
                if(s.getId() == s2.getId())
                    s.setIntensity(s2.getIntensity());
            }
        }
	}

}