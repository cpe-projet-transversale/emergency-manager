package controller;

import java.util.Timer;
import java.util.TimerTask;

import model.Position;
import task.*;

public class EmergencyManager {

    public EmergencyManager(){

    }

    public static void main(String[] args) {
        /**
         * Délai d'attente entre chaque tâche, en millisecondes
         */
        int delayForTask = 2000;
        int numberOfTask = 3;

        GameManager gameManager = new GameManager();

        TimerTask checkFireDeathTask = new checkFireDeath(gameManager);
        //TimerTask checkCrewReturnTask = new checkCrewReturn(gameManager);
        TimerTask checkNewFireTask = new checkNewFire(gameManager);
        TimerTask updateDatabaseTask = new updateDatabase(gameManager);
        Timer timer = new Timer(true); //On utilise qu'un seul timer pour qu'elle s'éxécute séquentiellement... Semble logique mais bonne idée pour les tours ? (durée d'éxécution de chaque code)
        timer.scheduleAtFixedRate(checkFireDeathTask, delayForTask - (numberOfTask)*(delayForTask/numberOfTask), delayForTask); //On commence la réduction des feux directement, puis toutes les 2 secondes
        //timer.scheduleAtFixedRate(checkCrewReturnTask, 0, delayForTask); //On commence la création des feux à partir de 2 secondes, toutes les deux secondes
        timer.scheduleAtFixedRate(checkNewFireTask, delayForTask - (numberOfTask-1)*(delayForTask/numberOfTask), delayForTask); 
        timer.scheduleAtFixedRate(updateDatabaseTask, delayForTask - (numberOfTask-2)*(delayForTask/numberOfTask), delayForTask); 

        try {
            Thread.sleep(120000); //On attends 60 ms avant de tout annuler pour stop le programme
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        timer.cancel();
    }
}